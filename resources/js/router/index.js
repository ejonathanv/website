import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Home from '../views/Home';
import About from '../views/About';

const router = new VueRouter({

	mode: 'history',
	routes: [

		{
			path: '/',
			component: Home,
			name: 'home'
		},
		{
			path: '/acerca-de-mi',
			component: About,
			name: 'about-me'
		}

	]

});

export default router;